
package atmsys;

import java.util.Random;

public class Quotes {
    String quote ;
    public String returnQuotes(){
    Random ran = new Random();
        int num = ran.nextInt(10);
        switch (num) {
            case 0:
               quote  = "To accomplish great things, we "
                        + "must not only act, but also dream, "
                        + "not only plan, but also believe.\n\n"
                        + "- Anatole France";
                break;
            case 1:
                quote  = "Victory is always possible for the "
                        + "person who refuses to "
                        + "stop fighting.\n\n"
                        + "- Napoleon Hill";
                break;
            case 2:
                quote  = "Great works are performed "
                        + "not by strength, "
                        + "but perseverance.\n\n"
                        + "- Dr. Samuel Johnson";
                break;
            case 3:
                quote  = "Society may predict, but only you "
                        + "can determine your destiny.\n\n"
                        + "- Clair Oliver";
                break;
            case 4:
                quote  = "Success comes from having dreams "
                        + "that are bigger than your fears.\n\n"
                        + "- Terry Litwiller";
                break;
            case 5:
                quote  = "Imagination is more important "
                        + "than knowledge.\n\n"
                        + "- Albert Einstein";
                break;

            case 6:
                quote  = "Without a goal, discipline is "
                        + "nothing but self-punishment.\n\n"
                        + "- Elanor Roosevelt";
            case 7:
                quote = "I sincerely believe that banking establishments" +
                        " are more dangerous than standing armies, and that" +
                        " the principle of spending money to be paid by posterity," +
                        " under the name of funding, is but swindling futurity " +
                        "on a large scale. - Thomas Jefferson";
                break;
            case 8:
                quote = "It is well enough that people of the nation do" +
                        " not understand our banking and monetary system," +
                        " for if they did, I believe there would be a" +
                        " revolution before tomorrow morning. - Henry Ford";
                break;
            case 9:
                quote = "I believe that banking institutions are" +
                        " more dangerous to our liberties " +
                        "than standing armies. - Thomas Jefferson";
        }
    return quote;
    }
}
