package atmsys;

import atmsys.controller.MainBoardController;
import atmsys.navigation.Navigation;
import javafx.application.Application;

import javafx.stage.Stage;

public class MainApp extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Navigation nav = Navigation.getInstance(stage);
        nav.load(MainBoardController.URL_FXML).Show();
    }
    public static void main(String[] args) {
        launch(args);
    }

}
