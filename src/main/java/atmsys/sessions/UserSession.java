package atmsys.sessions;

public final class UserSession {
    private static UserSession instance;

    private String userName;
    private Privilege privilege;

    private UserSession(String userName, Privilege privilege) {
        this.userName = userName;
        this.privilege = privilege;
    }

    private UserSession() {
        userName = "";
        privilege = Privilege.None;
    }

    public static UserSession getInstance(String userName, Privilege privilege) {
        instance = new UserSession(userName, privilege);

        return instance;
    }

    public static UserSession getInstance() {
        if (instance == null) {
            instance = new UserSession();
        }
        return instance;
    }

    public String getUserName() {
        return userName;
    }

    public Privilege getPrivilege() {
        return privilege;
    }

    public void cleanUserSession() {
        userName = "";
        privilege = Privilege.None;
    }

    @Override
    public String toString() {
        return "UserSession{ " +
                "userName='" + userName + '\'' +
                ", privileges=" + privilege.toString() + '}';
    }


}
