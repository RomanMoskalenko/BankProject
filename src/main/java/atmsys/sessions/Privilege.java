package atmsys.sessions;

public enum Privilege {
    None,
    User,
    Admin
}
