package atmsys.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import atmsys.CustomerData;
import atmsys.DbConnection;
import atmsys.navigation.BaseController;
import atmsys.navigation.Navigation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AdminPageController extends BaseController implements Initializable {
    static final String URL_FXML = "/fxml/AdminPage.fxml";

    @FXML
    private Button loadcusinfo;

    @FXML
    private TextField num;

    @FXML
    private TableView<CustomerData> customertable;

    @FXML
    private TableColumn<CustomerData,Integer> cusid;

    @FXML
    private TableColumn<CustomerData,String> cusname;

    @FXML
    private TableColumn<CustomerData,String> cusaddress;

    @FXML
    private TableColumn<CustomerData,String> cusemail;

    @FXML
    private TableColumn<CustomerData,String> cusphone;

    @FXML
    private TableColumn<CustomerData,Integer> cusbalance;

    private ObservableList<CustomerData> data;

    @FXML
    private Label welcome;

    private String AdminID;

    @FXML
    private ImageView adminImage;

    @FXML
    private Label adminName;

    @FXML
    private Label adminId;

    void getAdminByID(String id) throws SQLException, FileNotFoundException, IOException{
        AdminID = id;
        Connection con = DbConnection.Connection();
        assert con != null;
        PreparedStatement ps = con.prepareStatement("SELECT * FROM admins WHERE id = ?");
        ps.setString(1, id);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            adminName.setText(rs.getString("name"));
            adminId.setText(rs.getString("id"));
            InputStream is = rs.getBinaryStream("image");
            File f = new File("/icons/adminimage.jpeg");
            //OutputStream os = new FileOutputStream(f);
            //byte[] content = new byte[1024];
            //int s = 0;
            //while((s= is.read(content))!= -1){
            //    os.write(content, 0, s);
            //}
            Image image = new Image("/icons/adminicon.jpeg");
            adminImage.setImage(image);
            adminImage.setFitWidth(248);
            adminImage.setFitHeight(186);
            Circle clip = new Circle(93,93,93);
            adminImage.setClip(clip);
        }
        ps.close();
        rs.close();
        con.close();
    }

    @FXML
    public void LoadCustomerData(ActionEvent event) throws SQLException{
    Connection con = DbConnection.Connection();
    data = FXCollections.observableArrayList();
    PreparedStatement ps = con.prepareStatement("SELECT * FROM users");
    ResultSet rs = ps.executeQuery();
    while(rs.next()){
    data.add(new CustomerData(rs.getInt("id"),rs.getString("name"),rs.getString("address"),rs.getString("email"),rs.getString("Phone"),rs.getInt("balance")));
    }
            cusid.setCellValueFactory(new PropertyValueFactory<>("CustomerId"));
            cusname.setCellValueFactory(new PropertyValueFactory<>("CustomerName"));
            cusaddress.setCellValueFactory(new PropertyValueFactory<>("CustomerAddress"));
            cusemail.setCellValueFactory(new PropertyValueFactory<>("CustomerEmail"));
            cusphone.setCellValueFactory(new PropertyValueFactory<>("CustomerPhone"));
            cusbalance.setCellValueFactory(new PropertyValueFactory<>("CustomerBalance"));
            customertable.setItems(null);
            customertable.setItems(data);
            ps.close();
            rs.close();
            con.close();
    }

    @FXML
    public void AddCustomer(ActionEvent event) throws IOException{
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/AddCustomer.fxml"));
        loader.load();
        Parent root = loader.getRoot();
        AddCustomerController acController = loader.getController();
        acController.setView(root);
        Scene scene = new Scene(root);
        Image icon = new Image("/icons/adduser.png");
        stage.getIcons().add(icon);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.setTitle("Add Customer Page");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    @FXML
    public void EditCustomerData(ActionEvent event) throws IOException{
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/EditCustomer.fxml"));
        loader.load();

        Parent root = loader.getRoot();
        EditCustomerController ecController = loader.getController();
        ecController.setView(root);
        Scene scene = new Scene(root);
        Image icon = new Image("/icons/edituser.png");
        stage.getIcons().add(icon);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.setTitle("Edit Customer Page");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    @FXML
    public void DeleteCustomerData(ActionEvent event) throws IOException{
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/DeleteCustomer.fxml"));
        loader.load();
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Image icon = new Image("/icons/deleteuser.png");
        stage.getIcons().add(icon);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.setTitle("Delete Customer Page");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    @FXML
    public void ResetPassword(ActionEvent event) throws IOException{
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/PasswordReset.fxml"));
        loader.load();
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/PasswordReset.css");
        Image icon = new Image("/icons/Password.png");
        stage.getIcons().add(icon);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.setTitle("Passwor Reset Page");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    @Override
    public void init() {
        Stage stage = (Stage)view.getScene().getWindow();

        stage.setTitle("Admin Page");
        Image icon = new Image("/icons/adminicon.jpeg");
        stage.getIcons().add(icon);
        stage.setHeight(700);
        stage.setWidth(1366);
        stage.setOnCloseRequest(event1 -> {
            event1.consume();
            Navigation nav1 = Navigation.getInstance();
            nav1.load(MainBoardController.URL_FXML).Show();
        });
        stage.show();

        //Media someSound = new Media(getClass().getResource("/audio/AdminLogin.wav").toString());
        //MediaPlayer mp = new MediaPlayer(someSound);
        //mp.play();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }
}
