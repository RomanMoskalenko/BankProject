package atmsys.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import atmsys.DbConnection;
import atmsys.Quotes;
import atmsys.navigation.BaseController;
import atmsys.navigation.Navigation;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class UserPageController extends BaseController implements Initializable {
    public static final String URL_FXML = "/fxml/UserPage.fxml";

    @FXML
    private Label welcome;
    @FXML
    private TextArea quotedis;

    private String UserID, UserName;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void init() {
        Stage stage = (Stage)view.getScene().getWindow();

        stage.setOnCloseRequest(event -> {

        });

        stage.setTitle("User Page");
        Image icon = new Image("/icons/UserPage.png");
        stage.getIcons().add(icon);
        stage.setWidth(1345);
        stage.setHeight(710);
        stage.setMaximized(true);

        stage.show();

        //Media someSound = new Media(getClass().getResource("/audio/UserLogin.wav").toString());
        //MediaPlayer mp = new MediaPlayer(someSound);
        //mp.play();
    }

    public void setUserID(String id, String Name) throws SQLException {
        welcome.setText(Name);
        UserID = id;
        UserName = Name;
        Quotes qt = new Quotes();
        String quote = qt.returnQuotes();
        quotedis.setText(quote);

    }

    public void checkBalance(ActionEvent event) throws SQLException, IOException {
        Connection con = DbConnection.Connection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ps = con.prepareStatement("SELECT * FROM users WHERE id = ? AND name = ?");
        ps.setString(1, UserID);
        ps.setString(2, UserName);
        rs = ps.executeQuery();
        while (rs.next()) {
            String Balance = Integer.toString(rs.getInt("balance"));
            Stage stage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/BalancePage.fxml"));
            loader.load();
            Parent root = loader.getRoot();
            BalancePageController bpc = loader.getController();
            bpc.SetBalance(Balance);
            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/BalancePage.css");
            Image icon = new Image("/icons/DepositPage.png");
            stage.getIcons().add(icon);
            stage.setResizable(false);
            stage.sizeToScene();
            stage.setTitle("Balance Page");
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        }
        ps.close();
        rs.close();
    }

    @FXML
    public void withdraw(ActionEvent event) throws IOException, SQLException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/WithdrawPage.fxml"));
        loader.load();
        Parent root = loader.getRoot();
        BalancePageController bpc = loader.getController();
        bpc.getUserID(UserID);
        bpc.StqWithdrawPage();
        Scene scene = new Scene(root);
        Image icon = new Image("/icons/DepositPage.png");
        stage.getIcons().add(icon);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.setTitle("Withdraw Page");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    @FXML
    void depositMoney(ActionEvent event) throws IOException, SQLException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/DepositPage.fxml"));
        loader.load();
        Parent root = loader.getRoot();
        BalancePageController bpc = loader.getController();
        bpc.getUserID(UserID);
        bpc.StqDepositPage();
        bpc.setView(root);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/DepositPage.css");
        Image icon = new Image("/icons/DepositPage.png");
        stage.getIcons().add(icon);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.setTitle("Deposite Page");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    @FXML
    void AccountInfo(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/AccountInfoPage.fxml"));
        loader.load();
        Parent root = loader.getRoot();
        AccountInfoController aic = loader.getController();
        aic.getUserID(UserID);
        aic.StqAccountInfo();
        Scene scene = new Scene(root);
        Image icon = new Image("/icons/usericon4.png");
        stage.getIcons().add(icon);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.setTitle("Account Information");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    @FXML
    void ChangePassword(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/ChangePassword.fxml"));
        loader.load();
        Parent root = loader.getRoot();
        AccountInfoController aic = loader.getController();
        aic.getUserID(UserID);
        aic.StqPasswordChangePage();
        Scene scene = new Scene(root);
        Image icon = new Image("/icons/LoginPage.png");
        stage.getIcons().add(icon);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.setTitle("Change Password");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }
    @FXML
    void BalanceTransfer(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/BalanceTransfer.fxml"));
        loader.load();
        Parent root = loader.getRoot();
        BalancePageController bpc = loader.getController();
        bpc.getUserID(UserID);
        bpc.StqBalanceTransPage();
        Scene scene = new Scene(root);
        Image icon = new Image("/icons/DepositPage.png");
        stage.getIcons().add(icon);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.setTitle("Balance Transfer");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    @FXML
    private void Back(ActionEvent event) {
        Stage stage = (Stage)view.getScene().getWindow();
        if (!stage.getScene().getStylesheets().isEmpty())
            stage.getScene().getStylesheets().remove(0,1);
        Navigation nav = Navigation.getInstance();
        nav.load(MainBoardController.URL_FXML).Show();
    }
}