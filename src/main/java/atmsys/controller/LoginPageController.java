package atmsys.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import atmsys.DbConnection;
import atmsys.navigation.BaseController;
import atmsys.navigation.Navigation;
import atmsys.sessions.Privilege;
import atmsys.sessions.UserSession;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class LoginPageController extends BaseController implements Initializable {
    public static final String URL_FXML = "/fxml/LoginPage.fxml";

    @FXML
    private TextField useridtf;
    @FXML
    private Label mssg;
    @FXML
    private Button loginb;
    @FXML
    private PasswordField passwordtf;
    @FXML
    private RadioButton userrb;
    @FXML
    private ToggleGroup UserOrAdmin;
    @FXML
    private RadioButton adminrb;

    @FXML
    private void Login(ActionEvent event) throws SQLException, IOException, URISyntaxException {
        Connection con = DbConnection.Connection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        ps = con.prepareStatement("SELECT * FROM users WHERE id = ? and password = ?");
        ps.setString(1, useridtf.getText());
        ps.setString(2, passwordtf.getText());
        rs = ps.executeQuery();
        if (rs.next()) {
            Navigation nav = Navigation.getInstance();
            nav.load(UserPageController.URL_FXML).Show();

            ((UserPageController)nav.load(UserPageController.URL_FXML))
                    .setUserID(useridtf.getText(), rs.getString("name"));
            UserSession session = UserSession.getInstance(rs.getString("name"), Privilege.User);

            useridtf.setText("");
            passwordtf.setText("");

            mssg.setText("");
        }
        else{
            mssg.setText("Wrong Password Or UserID");
        }
        ps.close();
        rs.close();

        con.close();
    }

    @FXML
    private void adminLogin(ActionEvent event) throws SQLException, IOException {
        Connection con = DbConnection.Connection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        ps = con.prepareStatement("SELECT * FROM admins WHERE id = ? and password = ?");
        ps.setString(1, useridtf.getText());
        ps.setString(2, passwordtf.getText());
        rs = ps.executeQuery();
        if(rs.next()) {


            Navigation nav = Navigation.getInstance();
            nav.load(AdminPageController.URL_FXML).Show();
            ((AdminPageController)nav.load(AdminPageController.URL_FXML))
                    .getAdminByID(useridtf.getText());
            UserSession session = UserSession.getInstance(rs.getString("name"), Privilege.Admin);

            mssg.setText("");
            useridtf.setText("");
            passwordtf.setText("");
        }
        else{
            mssg.setText("Wrong Password Or AdminID");
        }
        ps.close();
        rs.close();
        con.close();
    }

    @FXML
    private void signUp(MouseEvent event) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/AddCustomer.fxml"));
        loader.load();
        Parent root = loader.getRoot();
        AddCustomerController acController = loader.getController();
        acController.setView(root);
        Scene scene = new Scene(root);
        Image icon = new Image("/icons/adduser.png");
        stage.getIcons().add(icon);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.setTitle("Add Customer Page");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    @FXML
    private void Back(ActionEvent event) {
        Navigation nav = Navigation.getInstance();
        nav.load(MainBoardController.URL_FXML).Show();
    }

    @Override
    public void init() {
        Stage stage = (Stage)view.getScene().getWindow();

        stage.setTitle("Login");
        Image icon = new Image("/icons/LoginPage.png");
        stage.getIcons().add(icon);
        stage.setResizable(false);
        stage.setHeight(500);
        stage.setWidth(800);
        stage.setOnCloseRequest(event -> {
            event.consume();
            Navigation nav = Navigation.getInstance();
            nav.load(MainBoardController.URL_FXML).Show();
        });
        stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}
