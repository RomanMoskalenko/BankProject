package atmsys.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.ResourceBundle;

import atmsys.DbConnection;
import atmsys.Quotes;
import atmsys.navigation.BaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class AccountInfoController extends BaseController implements Initializable {
    public static final String URL_FXML = "/fxml/AccountInfoPage.fxml";

    String UserID;
    @FXML
    private TextField uid;
    @FXML
    private TextField uname;
    @FXML
    private TextArea uaddress;
    @FXML
    private TextField uemail;
    @FXML
    private TextField uphone;
    @FXML
    private TextField ubalance;
    @FXML
    private Label welcome;
    @FXML
    private PasswordField oldpass;
    @FXML
    private PasswordField newpass;
    @FXML
    private PasswordField passretype;
    @FXML
    private Label changepassconf;
    @FXML
    private TextArea quotediscp;
    @FXML
    private TextArea quotedisai;

    public void getUserID(String Id) {
        UserID = Id;
    }

    public void StqPasswordChangePage() {
        Quotes qt = new Quotes();
        String quote = qt.returnQuotes();
        quotediscp.setText(quote);
    }

    public void StqAccountInfo() {
        Quotes qt = new Quotes();
        String quote = qt.returnQuotes();
        quotedisai.setText(quote);
    }
    
    public void AccountInfo(ActionEvent event) throws SQLException{
        Node node  = (Node) event.getSource();
        Scene scene = (Scene) node.getScene();
        AnchorPane anchorPane = (AnchorPane) scene.getRoot();
        Pane pane = (Pane) anchorPane.getChildren().get(0);
        TextField textField1 = (TextField)pane.getChildren().get(1);
        int id = 0;
        try {
            id = Integer.parseInt(textField1.getText());
        } catch (NumberFormatException nfe) {
            System.out.println("NumberFormatException");
            return;
        }

        Connection con = DbConnection.Connection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ps = con.prepareStatement("SELECT * FROM users WHERE id = ?");
        ps.setString(1, textField1.getText());
        rs = ps.executeQuery();
        while(rs.next()){

            Label label1 = (Label)pane.getChildren().get(0);
            label1.setLayoutX(35);
            label1.setLayoutY(39);


            textField1.setLayoutX(220);
            textField1.setLayoutY(30);

            int i=0;
            Iterator<Node> it = pane.getChildren().iterator();
            while(true){


                if (i > 3)
                    it.remove();

                if (!it.hasNext()) {
                    break;
                }
                it.next();
                i++;
            }

            Label label2 = new Label();
            label2.setLayoutX(35);
            label2.setLayoutY(104);
            label2.setStyle("-fx-text-fill : black;");
            label2.setText("Username");
            label2.setStyle("-fx-background-color: transparent");
            pane.getChildren().add(label2);

            TextField textField2 = new TextField();
            textField2.setLayoutX(220);
            textField2.setLayoutY(95);
            textField2.setPrefHeight(45);
            textField2.setPrefWidth(300);
            textField2.setEditable(false);
            textField2.setStyle("-fx-background-color: transparent");
            textField2.setText(rs.getString("name"));
            pane.getChildren().add(textField2);

            if (String.valueOf(id).equals(UserID)) {
                Label label3 = new Label();
                label3.setLayoutX(35);
                label3.setLayoutY(189);
                label3.setStyle("-fx-text-fill : black;");
                label3.setText("Address");
                pane.getChildren().add(label3);

                TextArea textArea = new TextArea();
                textArea.setLayoutX(222);
                textArea.setLayoutY(160);
                textArea.setWrapText(false);
                textArea.setPrefHeight(85);
                textArea.setPrefWidth(268);
                textArea.setStyle("-fx-text-fill: black; -fx-border-color: #82d3d5;\n" +
                        "    -fx-border-radius: 4;\n" +
                        "    -fx-border-width: 2;");
                textArea.setEditable(false);
                textArea.setText(rs.getString("address"));
                pane.getChildren().add(textArea);

                Label label4 = new Label();
                label4.setLayoutX(35);
                label4.setLayoutY(274);
                label4.setStyle("-fx-text-fill : black;");
                label4.setText("Email");
                pane.getChildren().add(label4);

                TextField textField4 = new TextField();
                textField4.setLayoutX(220);
                textField4.setLayoutY(265);
                textField4.setPrefHeight(45);
                textField4.setPrefWidth(300);
                textField4.setEditable(false);
                textField4.setStyle("-fx-background-color: transparent");
                textField4.setText(rs.getString("email"));
                pane.getChildren().add(textField4);


                Label label5 = new Label();
                label5.setLayoutX(35);
                label5.setLayoutY(339);
                label5.setStyle("-fx-text-fill : black;");
                label5.setText("Phone");
                pane.getChildren().add(label5);

                TextField textField5 = new TextField();
                textField5.setLayoutX(220);
                textField5.setLayoutY(330);
                textField5.setPrefHeight(45);
                textField5.setPrefWidth(300);
                textField5.setEditable(false);
                textField5.setStyle("-fx-background-color: transparent");
                textField5.setText(rs.getString("phone"));
                pane.getChildren().add(textField5);

                Label label6 = new Label();
                label6.setLayoutX(35);
                label6.setLayoutY(404);
                label6.setStyle("-fx-text-fill : black;");
                label6.setText("Current Balance");
                pane.getChildren().add(label6);

                TextField textField6 = new TextField();
                textField6.setLayoutX(220);
                textField6.setLayoutY(395);
                textField6.setPrefHeight(45);
                textField6.setPrefWidth(300);
                textField6.setEditable(false);
                textField6.setStyle("-fx-background-color: transparent");
                textField6.setText(rs.getString("balance"));
                pane.getChildren().add(textField6);
            }
        }
        ps.close();
        rs.close();
        con.close();
    }

   public void ChangePassword(ActionEvent event) throws SQLException{
       if(!newpass.getText().equals(passretype.getText())){
       changepassconf.setText("Password Confirmation Failed");
       passretype.setText("");
       passretype.setStyle("-fx-border-color:red;-fx-border-width:2;-fx-border-radius:20;-fx-background-radius:22;");
       }
       else if(oldpass.getText().isEmpty()||newpass.getText().isEmpty()||passretype.getText().isEmpty()){
       changepassconf.setText("Please Fill Up Everything Correctly.");
       }
       else{
       Connection con = DbConnection.Connection();
       PreparedStatement ps = con.prepareStatement("UPDATE users SET password = ? WHERE id ='"+UserID+"' AND password ='"+oldpass.getText()+"'");
       ps.setString(1, newpass.getText());
       int i = ps.executeUpdate();
       if(i>0){
       changepassconf.setText("Password Changed.");
       }
       else{
       changepassconf.setText("Wrong Password.");
       }
       oldpass.setText("");
       newpass.setText("");
       passretype.setText("");
       passretype.setStyle("-fx-border-color: #3b5998;-fx-border-width:2;-fx-border-radius:20;-fx-background-radius:22;");
       ps.close();
       con.close();
       }
   }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}
