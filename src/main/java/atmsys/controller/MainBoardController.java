package atmsys.controller;

import atmsys.navigation.Navigation;
import atmsys.sessions.Privilege;
import atmsys.navigation.BaseController;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import atmsys.sessions.UserSession;

public class MainBoardController extends BaseController implements Initializable {
    public static final String URL_FXML = "/fxml/MainBoard.fxml";

    @FXML
    Button homeb;

    @FXML
    Button loginb;

    @FXML
    Button joinb;

    @FXML
    private void Login(ActionEvent event) throws IOException {
        Navigation nav = Navigation.getInstance();
        nav.load(LoginPageController.URL_FXML).Show();
    }

    @FXML
    private void LogOut(ActionEvent event) {
        UserSession session = UserSession.getInstance("",Privilege.None);
    }

    @FXML
    public void Join(ActionEvent event) throws IOException{
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/AddCustomer.fxml"));
        loader.load();
        Parent root = loader.getRoot();
        AddCustomerController acController = loader.getController();
        acController.setView(root);
        Scene scene = new Scene(root);
        Image icon = new Image("/icons/adduser.png");
        stage.getIcons().add(icon);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.setTitle("Add Customer Page");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    @FXML
    private void Home(ActionEvent event) {
        UserSession session = UserSession.getInstance();
        if (session.getPrivilege() == Privilege.None) {
            Navigation nav = Navigation.getInstance();
            nav.load(LoginPageController.URL_FXML).Show();

        } else if ((session.getPrivilege() == Privilege.User))  {
            Navigation nav = Navigation.getInstance();
            nav.load(UserPageController.URL_FXML).Show();
        } else if ((session.getPrivilege() == Privilege.Admin))  {
            Navigation nav = Navigation.getInstance();
            nav.load(AdminPageController.URL_FXML).Show();
        }
    }

    @Override
    public void init() {
        Stage stage = (Stage)view.getScene().getWindow();

        Image icon = new Image("/icons/LoginPage.png");
        stage.getIcons().add(icon);
        stage.setWidth(1000);
        stage.setHeight(600);
        stage.setResizable(false);
        stage.setTitle("Login");
        stage.setOnCloseRequest(event -> {
            System.out.println("Goodbye");
        });
        stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Media someSound = new Media(getClass().getResource("/audio/Welcome.wav").toString());
        //MediaPlayer mp = new MediaPlayer(someSound);
        //mp.play();
    }
}
