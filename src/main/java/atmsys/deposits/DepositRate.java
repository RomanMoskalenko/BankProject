package atmsys.deposits;

public class DepositRate {
    private String name;
    private String description;
    private double interestRate;
    private boolean isReplacement;
    private int minimalTerm;
    private double renewalAllowance;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public boolean isReplacement() {
        return isReplacement;
    }

    public void setReplacement(boolean replacement) {
        isReplacement = replacement;
    }

    public int getMinimalTerm() {
        return minimalTerm;
    }

    public void setMinimalTerm(int minimalTerm) {
        this.minimalTerm = minimalTerm;
    }

    public double getRenewalAllowance() {
        return renewalAllowance;
    }

    public void setRenewalAllowance(double renewalAllowance) {
        this.renewalAllowance = renewalAllowance;
    }
}
