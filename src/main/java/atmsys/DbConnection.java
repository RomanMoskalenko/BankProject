
package atmsys;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbConnection {
    public static Connection Connection() throws SQLException{
        try {
            Class.forName("org.sqlite.JDBC");
            URL resource = DbConnection.class
                    .getResource("/db/ams.sqlite");
            String path = new File(resource.toURI()).getAbsolutePath();

            Connection conn = DriverManager.getConnection(String.format("jdbc:sqlite:%s",path));
            return conn;
        } catch (ClassNotFoundException | URISyntaxException ex) {
            Logger.getLogger(DbConnection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
