package atmsys.navigation;

import javafx.scene.Node;

public class BaseController implements Controller {

    protected Node view;

    @Override
    public Node getView() {
        return view;
    }

    @Override
    public void setView (Node view){
        this.view = view;
    }

    @Override
    public void Show() {
        PreShowing();
        Navigation.getInstance().Show(this);
        PostShowing();
    }

    public void PreShowing()
    {

    }

    public void PostShowing()
    {

    }

    @Override
    public void init() {

    }
}
