package atmsys.navigation;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


import java.util.HashMap;
import java.util.Map;

public class Navigation {

    private final Stage stage;
    private final Scene scene;

    private Map<String, Controller> controllerMap = new HashMap<>();

    private static Navigation instance;

    private Navigation(Stage stage)
    {
        this.stage = stage;
        scene = new Scene(new Pane());
        stage.setScene(scene);
    }

    public static Navigation getInstance(Stage stage) {
        if (instance == null)
            instance = new Navigation(stage);
        return instance;
    }

    public static Navigation getInstance() {
        return instance;
    }

    public Controller load(String sUrl)
    {
        try {
            if (controllerMap.containsKey(sUrl))
                return controllerMap.get(sUrl);
            //loads the fxml file
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(sUrl));
            Node root = fxmlLoader.load();

            Controller controller = fxmlLoader.getController();
            controller.setView(root);

            controllerMap.put(sUrl, controller);

            return controller;

        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void Show(Controller controller)
    {
        try {
            scene.setRoot((Parent) controller.getView());
            controller.init();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

}

