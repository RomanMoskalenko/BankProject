package atmsys.navigation;

import javafx.scene.Node;

public interface Controller {
    Node getView();
    void setView(Node view);
    void init();
    void Show();
}
